<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gurudwara extends Model
{
    protected $fillable = ['name', 'image', 'info']; 

    public function comments() {
    	return $this->hasMany(Comment::class)->orderBy('created_at', 'desc');
    }
           
}
