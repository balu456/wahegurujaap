<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ragi extends Model
{
    protected $fillable = ['name', 'image', 'description'];   

    public function comments() {
    	return $this->hasMany(Comment::class)->orderBy('created_at', 'desc');
    }

    public function videos() {
    	return $this->hasMany(Video::class)->orderBy('created_at', 'desc');
    }
         
}
