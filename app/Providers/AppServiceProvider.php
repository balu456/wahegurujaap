<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $count_jaap = DB::table('users')->pluck('jaap');

        $total = 0;

        foreach ($count_jaap as $count) {
            $total += $count;
        }

        $users = DB::table('users')->get();

        View::share('jaaps', $total);
        View::share('users', $users);

        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
