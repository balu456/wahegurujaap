<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = ['title', 'image'];

    public function comments() {
    	return $this->hasMany(Comment::class)->orderBy('created_at', 'desc');
    }
    
}
