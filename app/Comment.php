<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Request;

class Comment extends Model
{
    protected $fillable = ['user_id', 'gallery_id', 'video_id', 'ragi_id', 'gurudwara_id', 'body', 'comment_type'];

    public function gallery() {
    	return $this->belongsTo(Gallery::class);
    }

    public function video() {
    	return $this->belongsTo(Video::class);
    }

    public function ragi() {
        return $this->belongsTo(Ragi::class);
    }

    public function gurudwara() {
        return $this->belongsTo(Gurudwara::class);
    }

    public function user() {
    	return $this->belongsTo(User::class);
    }

}
