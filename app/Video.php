<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = ['ragi_id', 'title', 'image', 'video_link', 'description'];

    public function comments() {
    	return $this->hasMany(Comment::class)->orderBy('created_at', 'desc');
    }

    public function ragi() {
    	return $this->belongsTo(Ragi::class);
    }
}
