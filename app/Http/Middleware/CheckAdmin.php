<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) { 
        // dd(auth()->id()->admin);
        $user = Auth::user(); 
        if ($user->isadmin != 1) {
            return redirect('/404');              
        }          
            return $next($request);
        } else {
                // dd('works');
                // Session::flash('message', 'Not a admin');                    
                return redirect('admin/login');            
        }

    }
}
