<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;

class CheckUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) { 
        // dd(auth()->id()->admin);
        $user = Auth::user(); 
        if ($user->active != 1) {
            return redirect('/');              
        }          
            return $next($request);
        } else {
                // dd('works');
                // Session::flash('message', 'Not a admin');                    
                return redirect('/login');            
        }

    }
}
