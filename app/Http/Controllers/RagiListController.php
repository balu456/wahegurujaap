<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Session;
use App\Ragi;
use App\Video;
use App\Gurudwara;

class RagiListController extends Controller
{
    public function index() {

    	$ragis = Ragi::all();

    	return view('ragilist.index', compact('ragis')); 
    
    }

    public function show($id) {

        $ragi = Ragi::findOrFail($id);

        $ragis = Ragi::all();

        $videos = Video::where('ragi_id', '=', $id)->get();

        $gurudwaras = Gurudwara::all();

    	return view('ragilist.show', compact('ragi', 'ragis', 'videos', 'gurudwaras'));

    }  

    public function add() {
        $this->validate(request(), [
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'image' => 'required|mimes:jpeg,bmp,png',
        ]);
        // dd(request()->all());
        If(Input::hasFile('image')){
// dd('works');
            $file = Input::file('image');

            $destinationPath = public_path(). '/assets/gallery/';
            $filename = $file->getClientOriginalName();

            $file->move($destinationPath, $filename);
            // dd($filename);
            // echo  $filename;
            //echo '<img src="uploads/'. $filename . '"/>';

            Ragi::create([
                'name' => request('name'),
                'image' => $filename,
                'description' => request('description'),                 
            ]);
        } else {
            dd(request('image'));
        }

        Session::flash('success', 'Ragi added');
        
        return back();
    } 

    public function edit($id) {
    	$ragi = Ragi::findOrFail($id);

    		return view('admin.edit-ragi', compact('ragi'));
    }

    public function update($id) {
		// dd(request()->all());
		$item = Ragi::findOrFail($id);
        // dd($item->title);

        If(Input::hasFile('image')){
        // dd(request()->all());

            $file = Input::file('image');

            $destinationPath = public_path(). '/assets/gallery/';
            $filename = $file->getClientOriginalName();

            $file->move($destinationPath, $filename);

            // echo  $filename;
            //echo '<img src="uploads/'. $filename . '"/>';

           $item->update(['name' => request('name'),'image' => $filename, 'description' => request('description') ]);

        } else {
                $item->update(['name' => request('name'), 'image' => $item->image, 'description' => request('description') ]);
        }

        Session::flash('success', 'Ragi info updated');
        
        return back();
    }

    public function delete($id) {
		
		$item = Ragi::findOrFail($id);
		
		$item->delete();

    	Session::flash('success', 'Ragi deleted');
    	
    	return back();
    }
}
