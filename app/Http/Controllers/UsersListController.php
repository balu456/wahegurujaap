<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;

class UsersListController extends Controller
{
    public function index() {
    	$users = User::all();
    	return view('users.index', compact('users')); 
    
    }

    public function show($user) {
    	$user = User::where('username', '=', "$user")->get()->first();
    
    	// dd($user->firstname);
    	return view('users.show', compact('user')); 
    
    }
}
