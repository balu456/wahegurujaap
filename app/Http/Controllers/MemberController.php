<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Hash;
use App\User;
use Input;
use Illuminate\Http\Request;

class MemberController extends Controller
{   
    public function index() {
        if (Auth::check()) {
            return view('users.profile');
        }
        return view('users.login');
    }

    public function authenticate()
    {
    	// dd('works');
    	$username = request('username');
    	$password = request('password');
    	// dd($password);
        if (Auth::attempt(['username' => $username, 'password' => $password])) {
            if ($user = Auth::attempt(['username' => $username, 'password' => $password, 'active' => 0])) {
            	Auth::Logout();
                return json_encode(array( 'message' => 'Please activate your account'));
            } else {
                $username = Auth::user()->username;
            	return json_encode(array('username' => $username, 'message' => 'success'));
            }  
        } else {
        	return json_encode(array( 'message' => 'User/Password does not match'));
        }
    }

    public function profile() {
        return view('users.profile');
    }

    public function comments($user) {
            $user = User::where('username', '=', $user)->first();
            return view('users.comments', compact('user'));
    }

    public function logout() {
        Auth::logout();
        Session::flush();
        return redirect('/login');
    }

    public function reset($user) {
        $this->validate(request(), [
            'old_password' => 'required|max:50',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password'
        ]);

         $current_password = Auth::User()->password; 
         // dd(request('password'));          
          if(Hash::check(request('old_password'), $current_password))
          {           
            $user_id = Auth::User()->id;                       
            $obj_user = User::find($user_id);
            $obj_user->password = Hash::make(request('new_password'));
            $obj_user->save(); 

            Session::flash('message', "Password has been changed.");

            return back();
            
          } else {

            Session::flash('message', "Current password doesn't match.");

            return back();

          }
    }

    public function update($id) {
        // dd(request()->all());
        $this->validate(request(), [
            'firstname' => 'required|string|max:255',
        ]);
        $user = User::where('username', '=', $id)->first();

        $user->firstname = request('firstname');
        if(request('lastname')!=null)
        $user->lastname = request('lastname');
        if(request('address')!=null)
        $user->address = request('address');
        if(request('country')!=null)        

        $user->country = request('country');
        if(request('state')!=null)        

        $user->state = request('state');
        if(request('city')!=null)        
        $user->city = request('city');
        if(request('zipcode')!=null)    
        $user->zipcode = request('zipcode');
        if(request('mobile')!=null)    
        $user->mobile = request('mobile');
        if(request('about')!=null)    
        $user->about = request('about');

        $user->save();

        // update(['firstname'=> request('firstname'), 'lastname' => request('lastname'), 'address' => request('address'), 'city' => request('city'), 'zipcode' => request('zipcode'), 'mobile' => request('mobile'), 'about' => request('about')]);
        Session::flash('message', "Updated successfully");

        return back();
        
    }

    public function upload($id) {
        $this->validate(request(), [
            'uploadImage' => 'required|image'
        ]);

        If(Input::hasFile('uploadImage')){

            $file = Input::file('uploadImage');

            $destinationPath = public_path(). '/assets/members/';
            $filename = $file->getClientOriginalName();

            $file->move($destinationPath, $filename);
            
            User::where('username', '=', $id)->update(['image'=> $filename]);

            Session::flash('message', 'Uploaded successfully');

            return back();

        }
    }
}
