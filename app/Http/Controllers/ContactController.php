<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Session;

class ContactController extends Controller
{
    public function index() {

    	return view('contact'); 
    
    }

    public function sendContact(Request $request) {

    	$this->validate(request(), [
	        'email' => 'required|email',
	        'message' => 'required',
	    ]);

        Mail::to('brkapoor11@gmail.com')->send(new Contact($request));

    	Session::flash('message', 'Thank you.');

    	return back();
    }
}
