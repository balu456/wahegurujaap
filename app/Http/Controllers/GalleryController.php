<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Input;
use Session;
use App\Gallery;
use App\Gurudwara;

class GalleryController extends Controller
{
    public function index() {

        $gallery = Gallery::all();

        $gurudwaras = Gurudwara::all();
    	
        return view('gallery.index', compact('gallery', 'gurudwaras'));

    }

    public function show($id) {

        $gallery = Gallery::findOrFail($id);

        $images = Gallery::all();

    	return view('gallery.show', compact('gallery', 'images'));

    }  

    public function add() {

        $this->validate(request(), [
            'title' => 'required|string|max:255',
            'image' => 'required|mimes:jpeg,bmp,png',
        ]);

    	If(Input::hasFile('image')){

            $file = Input::file('image');

            $destinationPath = public_path(). '/assets/gallery/';
            $filename = $file->getClientOriginalName();

            $file->move($destinationPath, $filename);

            // echo  $filename;
            //echo '<img src="uploads/'. $filename . '"/>';

            $user = Gallery::create([
            	'title' => request('title'),
                'image' => $filename,          
            ]);
        } else {
        	dd(request('image'));
        }

    	Session::flash('success', 'Gallery item added');
    	
    	return back();
    } 

    public function edit($id) {
    	$gallery = Gallery::findOrFail($id);

    		return view('admin.edit-gallery', compact('gallery'));
    }

    public function update($id) {

        $this->validate(request(), [
            'title' => 'required|string|max:255',
            // 'image' => 'required|mimes:jpeg,bmp,png',
        ]);
		
		$item = Gallery::findOrFail($id);
		// dd($item->title);

    	If(Input::hasFile('image')){
    	// dd(request()->all());

            $file = Input::file('image');

            $destinationPath = public_path(). '/assets/gallery/';
            $filename = $file->getClientOriginalName();

            $file->move($destinationPath, $filename);

            // echo  $filename;
            //echo '<img src="uploads/'. $filename . '"/>';

           $item->update(['title' => request('title'), 'image' => $filename]);

    	} else {
    			$item->update(['title' => request('title'), 'image' => $item->image]);
    	}

    	Session::flash('success', 'Gallery item updated');
    	
    	return back();
    }

    public function delete($id) {
		
		$item = Gallery::findOrFail($id);
		
		$item->delete();

    	Session::flash('success', 'Gallery item deleted');
    	
    	return back();
    }


}
