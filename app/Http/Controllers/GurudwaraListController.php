<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Session;
use App\Gurudwara;

class GurudwaraListController extends Controller
{
    public function index() {

    	$gurudwaras = Gurudwara::all();

    	return view('gurudwaralist.index', compact('gurudwaras')); 
    
    }

    public function show($id) {
        $gurudwara = Gurudwara::findOrFail($id);

        $gurudwaras = Gurudwara::all();

    	return view('gurudwaralist.show', compact('gurudwara', 'gurudwaras'));

    }  

    public function add() {
        // dd(request()->all());
        If(Input::hasFile('image')){

            $file = Input::file('image');

            $destinationPath = public_path(). '/assets/gallery/';
            $filename = $file->getClientOriginalName();

            $file->move($destinationPath, $filename);
            // dd($filename);
            // echo  $filename;
            //echo '<img src="uploads/'. $filename . '"/>';

            Gurudwara::create([
                'name' => request('name'),
                'image' => $filename,
                'info' => request('description'),                 
            ]);
        } else {
            dd(request('image'));
        }

        Session::flash('success', 'Item added');
        
        return back();
    } 

    public function edit($id) {
    	$gurudwara = Gurudwara::findOrFail($id);

    		return view('admin.edit-gurudwara', compact('gurudwara'));
    }

    public function update($id) {
		// dd(request()->all());
		$item = Gurudwara::findOrFail($id);
        // dd($item->title);

        If(Input::hasFile('image')){
        // dd(request()->all());

            $file = Input::file('image');

            $destinationPath = public_path(). '/assets/gallery/';
            $filename = $file->getClientOriginalName();

            $file->move($destinationPath, $filename);

            // echo  $filename;
            //echo '<img src="uploads/'. $filename . '"/>';

           $item->update(['name' => request('name'),'image' => $filename, 'info' => request('description') ]);

        } else {
                $item->update(['name' => request('name'), 'image' => $item->image, 'info' => request('description') ]);
        }

        Session::flash('success', 'Item info updated');
        
        return back();
    }

    public function delete($id) {
		
		$item = Gurudwara::findOrFail($id);
		
		$item->delete();

    	Session::flash('success', 'Item deleted');
    	
    	return back();
    }
}
