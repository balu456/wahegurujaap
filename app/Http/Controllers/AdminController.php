<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
Use App\User;
use Session;

class AdminController extends Controller
{
    public function index() {
    	$email = request('email');
    	$password = request('password');
    	if (!Auth::attempt(['username' => $email, 'password' => $password, 'isadmin' => 1])) {
            Session::flash('message', "User/password doesn't match");
    		return back();
    		// dd('logged in');

    	} else {
            return redirect('admin/dashboard');                         	
    	}
    }

    public function logout() {
        Auth::logout();
        Session::flush();
        return redirect('admin/login');
    }
}
