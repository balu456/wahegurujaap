<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Session;

class ProductController extends Controller
{
    public function index() {

    	$products = Product::all();

    	return view('shop.index', compact('products'));
    }

    public function addToCart($id) {

    	$p = Product::find($id);
// \Cart::destroy();
// Session::flush();

        $name = $p->title;
        $price = $p->price;
        $qty = 1;
      
       \Cart::add($id, $name, 1, $price, array());
       
        return back();
    }

    public function updateCart(Request $request, $id) {
        \Cart::update($id, $request->qty);
        return back();
    }

    public function destroyCart($id) {
        \Cart::remove($id);
        return back();
    }
}
