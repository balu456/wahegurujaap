<?php

namespace App\Http\Controllers;

use App\Mail\Activation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Session;
use App\User;
use Auth;

class RegisterController extends Controller
{
    public function index() {
    	return view('auth.register');
    }

    public function store() {

    	$this->validate(request(), [
	        'firstname' => 'required|string|max:255',
            'username' => 'required|string|unique:users',
            // 'mobile' => 'integer|min:10 & max:10',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
	    ]);

    	$user = new User;

    	$user->firstname = request('firstname');
    	if(request('lastname') != null)
    	$user->lastname = request('lastname');
    	$user->email = request('email');
    	$user->username = request('username');
    	$user->password = bcrypt(request('password'));
    	if(request('mobile') != null)    	
    	$user->mobile = request('mobile');
    	$user->token = str_random(40);
    	$user->save();

    	 Mail::to($user->email)->send(new Activation($user));

    	Session::flash('message', 'Please verify your account with your email.');   

    	return back(); 	
    }

    public function activate($token) {
    	$user = User::where('token', '=', $token)->first();
    	
    	if($user->active==1) {    		
    		
    		return redirect('/');

    	}
    	
    	$user = User::where('token', '=', $token)->update(['active'=> 1]);
    	
    	Session::flash('message', 'Your account has been activated');

    	return view('users.activated');
    }

    public function activation() {
        // dd(request('activation_link'));
        $user = User::where('token', '=', request('activation_link'))->first();
        if($user==null) {
            Session::flash('message', 'No user associated with this account');    
            return back(); 
        } else {
            $user = User::where('token', '=', request('activation_link'))->first();
        
            if($user->active==1) {          
                Session::flash('message', 'Your account is already activated');
                return back();

            }
            $user = User::where('token', '=', request('activation_link'))->update(['active'=> 1]);
                
            Session::flash('message', 'Your account has been activated');

            return back();
        }        
    }
}
