<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Session;
use App\Video;
use App\Ragi;
use App\Gurudwara;

class VideoController extends Controller
{
    public function index() {

        $videos = Video::all();

        $gurudwaras = Gurudwara::all();

    	return view('videos.index', compact('videos', 'gurudwaras'));

    }

    public function show($id) {

        $video = Video::findOrFail($id);

        $videos = Video::all();

    	return view('videos.show', compact('video', 'videos'));

    }  

    public function add() {

        $this->validate(request(), [
            'title' => 'required|string|max:255',
            'video_link' => 'required|string|max:500',
            // 'description' => 'required|string|max:255',
            'image' => 'required|mimes:jpeg,bmp,png',
            
        ]);
        // dd(request()->all());
        // $ragi = Ragi::findorFail(request('id'));
        If(Input::hasFile('image')){
            $file = Input::file('image');

            $destinationPath = public_path(). '/assets/gallery/';
            $filename = $file->getClientOriginalName();

            $file->move($destinationPath, $filename);
            // dd($filename);
            // echo  $filename;
            //echo '<img src="uploads/'. $filename . '"/>';

            Video::create([
                'ragi_id' => request('ragi_id'),
                'title' => request('title'),
                'image' => $filename,
                'description' => request('description'),                 
                'video_link' => request('video_link'),
            ]);
        } else {
            dd(request('image'));
        }

        Session::flash('success', 'Video item added');
        
        return back();
    } 

    public function edit($id) {
    	$video = Video::findOrFail($id);
        $ragis = Ragi::all();

    		return view('admin.edit-video', compact('video', 'ragis'));
    }

    public function update($id) {
		// dd(request()->all());
		$item = Video::findOrFail($id);
        // dd($item->title);

        If(Input::hasFile('image')){
        // dd(request()->all());

            $file = Input::file('image');

            $destinationPath = public_path(). '/assets/gallery/';
            $filename = $file->getClientOriginalName();

            $file->move($destinationPath, $filename);

            // echo  $filename;
            //echo '<img src="uploads/'. $filename . '"/>';

           $item->update(['ragi_id' => request('ragi_id'), 'title' => request('title'), 'video_link' => request('video_link'), 'image' => $filename, 'description' => request('description') ]);

        } else {
                $item->update(['ragi_id' => request('ragi_id'), 'title' => request('title'), 'video_link' => request('video_link'), 'image' => $item->image, 'description' => request('description') ]);
        }

        Session::flash('success', 'Video item updated');
        
        return back();
    }

    public function delete($id) {
		
		$item = Video::findOrFail($id);
		
		$item->delete();

    	Session::flash('success', 'Video item deleted');
    	
    	return back();
    }

}
