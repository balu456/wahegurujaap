<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
Use App\User;
use Session;

class AdminLogin extends Controller
{
    public function index() {
    	$email = request('email');
    	$password = request('password');
    	if (!Auth::attempt(['email' => $email, 'password' => $password, 'isadmin' => 1])) {
    		// Auth::login($user, true);
            Session::flash('message', 'Not a admin');
    		return back();
    		// dd('logged in');

    	} else {
            return redirect('admin/dashboard');                         	
    	}
    }
}
