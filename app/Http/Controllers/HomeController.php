<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Auth;
use Session;
use Illuminate\Http\Request;
use App\User;
use App\Jaap;
use App\Gurudwara;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        $users = DB::table('users')->orderBy('created_at', 'desc')->get();
        
        $winner = DB::table('users')->orderBy('jaap', 'desc')->get()->first();

        $gurudwaras = DB::table('gurudwaras')->orderBy('created_at', 'desc')->limit(9)->get();
        return view('home', compact('users', 'winner', 'gurudwaras'));
    }

    public function jaap()
    {
        $jaap = substr_count(request('jaap'), 'waheguru');
        $user = Auth::user();
        $user->jaap = $user->jaap + $jaap;
        $user->save();

        $t_jaap = new Jaap;

        $t_jaap->user_id = $user->id;

        $t_jaap->jaap = $jaap;

        $t_jaap->save();

        Session::flash('message', "Your Jaap Recorded Successfully!");    
        return redirect('/profile/'.$user->username.'/jaap');
    }
}
