<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jaap')->default(0);
            $table->string('firstname');
            $table->string('lastname')->default('');
            $table->string('username');
            $table->string('email')->unique();
            $table->string('password');
            $table->date('dob')->default(date('y-m-d'));
            $table->string('address')->default('');            
            $table->string('country')->default('');            
            $table->string('state')->default('');            
            $table->string('city')->default('');            
            $table->string('about')->default('');            
            $table->integer('zipcode')->default(0); 
            $table->string('mobile')->default(0);                    
            $table->string('token');
            $table->boolean('active')->default(0);
            $table->boolean('isadmin')->default(0);
            $table->string('image')->default('');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
