<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::create([
        	'firstname' => 'balu',
        	'email' => 'abc@doe.com',
        	'username' => 'balu',
        	'password' => bcrypt('123456'),
        	'active' => 1,
        	'isadmin' => 1,
        	'rememberToken' => 'VOgONKdN9VmCn9noSK8XfBUkyq1zGRxvDVS'
        ]);

        $user->save();
    }
}
