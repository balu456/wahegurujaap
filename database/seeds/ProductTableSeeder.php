<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = App\Product::create([
        	'image' => 'https://reprog.files.wordpress.com/2010/03/coders-at-work-cover.png',
        	'title' => 'Coders',
        	'description' => 'code yourself',
        	'price' => 50
        ]);

        $product->save();

        $product = App\Product::create([
        	'image' => 'https://it-ebooks24.com/images/books/linux_system_administration_recipes_a_problem-solution_approach.jpg',
        	'title' => 'linux admin',
        	'description' => 'command line interface',
        	'price' => 55
        ]);

        $product->save();

        $product = App\Product::create([
        	'image' => 'http://www.studybazar.com/img/image/cache/catalog/Books/9788187325727-300x300.jpg',
        	'title' => 'php development',
        	'description' => 'develop any website',
        	'price' => 60
        ]);

        $product->save();


    }
}
