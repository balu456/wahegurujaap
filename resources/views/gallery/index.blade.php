@extends ('layouts.master')

@section ('content')
<h2>Gurudwara Gallery</h2>
<div class="clear"></div>
<!--Start Left Box-->
<div class="left-box">
@if(count($gallery)>0)
	@foreach($gallery as $item)
	<div class="main_gallery"><a href="{{ url('/gallery/' . $item->id) }}"><img src="{{ asset('/assets/gallery/' . $item->image) }}" class="photo_gallery" alt=""/></a><a href="{{ url('/gallery/' . $item->id) }}"><span>{{ $item->title }}</span></a></div>
	@endforeach
	@else
	<p>No item found.</p>
@endif
</div>

<div class="right-box">
<div class="title">Gurdwaras</div>
<ul class="nav nav-list">
	@if(count($gurudwaras))
		@foreach($gurudwaras as $gurudwara)
			<li><a href="{{ url('/gurudwara/' . $gurudwara->id) }}">{{ $gurudwara->name }}</a></li>
		@endforeach
	@endif
</ul>
</div></div>
@endsection