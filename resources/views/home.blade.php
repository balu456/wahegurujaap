@extends ('layouts.master')

@section ('content')
<div class="left-slider">
        <div class="carousel slide" id="my-carousel">
            <ol class="carousel-indicators">
                <li data-target="#my-carousel" data-slide-to="0" class="active"></li>
                <li data-target="#my-carousel" data-slide-to="1"></li>
                <li data-target="#my-carousel" data-slide-to="2"></li>
                <li data-target="#my-carousel" data-slide-to="3"></li>
            </ol>
        
            <div class="carousel-inner">
                <div class="item active">
                <img src="{{ asset('wahegurujaap/resources/front/images/banner1.jpg') }}" width="900" alt=""/>                  <!--<div class="carousel-caption">
                        <h4>Some title</h4>
                        <p>Some description.</p>
                    </div>-->
                </div>
                <div class="item">
                <img src="{{ asset('wahegurujaap/resources/front/images/banner2.jpg') }}" alt=""/>              </div>
            </div>
            

        </div>
<div class="shadow"></div>
</div>

<style>



.right-bar {
float: left;
width: 100%;
overflow: hidden;
margin: 0px 6px 10px 0px;
background: #fff;
}

.w-box {
float: left;
border: 1px solid #ccc;
padding: 5px 0px 0px 0px;
margin: 0px 0px 0px 0px;
border-top: 0;
width: 99%;
}

@media screen and (max-width: 960px){

#right_main{width:100%;float:left;}

}


</style>

<!--Start Right-bar-->
<div id="right_main">
<div class="right-bar">
<div class="title">Contest</div>
<div class="w-box">
<!--Start Inner Left-->
<div class="inner-left">
<font>Winner of the week</font>
@if(count($winner))
<p><a href="{{ url('user/' . $winner->username ) }}">{{ $winner->firstname }} </a> </p>
<p><font>From Jun 19 To Jun 25</font></p>
<p>Total jaap:{{ $winner->jaap }}</p>
<a href="{{ url('user/' . $winner->username ) }}"><img src="{{ asset('/assets/members/' . $winner->image) }}" class="img-polaroid" width="50" alt=""/></a>
@endif
</div>
<!--End Inner Left-->
<!--Start Inner Right-->
<div class="inner-right">
<font>In the Race of Winners</font>
<ul>
<li><a href="user/index/godzillamib.html">Balu Kapoor</a><span>1498</span></li><li><a href="user/index/brkapoor11.html">Balu Kapoor</a><span>1</span></li></ul>
</div>
<!--End Inner Right-->
</div>
</div>
<!--End Right-bar-->
<!--Start Right-bar-->
<div class="right-bar">
<div class="title">Recent Users</div>
<div class="w-box">

@if($users)
    @foreach($users as $user)
        <div class="users">
        <a href="{{ url('/user') }}/{{ $user->username }}"><img src="{{ asset('/assets/members/' . $user->image) }}" class="img-polaroid user_img" alt=""/></a><p style="width:55px; text-align:center;"><a href="{{ url('/user') }}/{{ $user->username }}">{{ $user->firstname }}</a></a></p>
        </div>
    @endforeach
    @else
    <p>No user found.</p> 
@endif
<div class="view_more">
<a href="{{ url('/users') }}">See All</a></div>
</div>
</div>
</div>
<!--End Right-bar-->
<!--Start Yellow-bar-->
<div class="yellow-bar">
<div class="left"></div>
<div class="right"></div>
</div>
<!--End Yellow-bar-->
<!--Start Left Box-->
<div class="left-box">
<h2><span>Start</span> Jaap Today !</h2>
<!--Start Buttons-->
<div class="buttons">
<div class="count">
<button class="btn btn-warning jaap" id="my_btn">0</button>
<p>Jaap</p>
</div>
<div class="count">
<button class="btn btn-warning mala" id="my_btn">0</button>
<p>Mala</p>
</div>
</div>
<!--End Buttons-->
<div class="vol">
<button class="btn"><i class="fa fa-volume-up"></i></button>
<button class="btn"><i class="fa fa-volume-off"></i><i class="fa fa-times"></i></button>
</div>
<form action="{{ url('/jaap') }}" id="submit_jaap" method="post" accept-charset="utf-8"><textarea name="jaap" id="jaap" class="jaap-field" placeholder="Start Jaap Today" ></textarea><ul class="nav pull-right">
{{ csrf_field() }}
<!-- <input type="hidden" name="jaap_count" value="" /> -->
<button disabled name="submit" id="my-button2" type="submit" class="btn btn-info" id="my-button2">Submit</button></ul>
</form></div>
<!--End Left Box-->
<!--Start Right Box-->
<div class="right-box">
<!--Start title-->
<div class="title">
    Gurudwaras List
</div>
@if(count($gurudwaras))
    @foreach($gurudwaras as $gurudwara)
        <div class="users" id="gurudwara">
        <a href="{{ url('/gurudwara/' . $gurudwara->id) }}"><img src="{{ asset('assets/gallery/' . $gurudwara->image) }}" class="img-rounded" width="79" alt=""></a><p><a href="{{ url('/gurudwara/' . $gurudwara->id) }}">{{ $gurudwara->name }}</a></p>
        </div>
    @endforeach
    @else
    <p>No item found.</p>
@endif
<!--End title-->
<div class="view_more">
<a href="{{ url('/gurudwaras') }}">See All</a></div>
</div>
<!--End Right Box-->

</div>
<!--End container-->


<!--####################################################### Start bg-testi-->
<div class="bg_testi">
<div class="container">
        <!-- TESTIMONIALS -->
        <h2>Messages</h2>
        
        <div class="carousel slide" id="testimonials">
            <ol class="carousel-indicators">
                <li data-target="#testimonials" data-slide-to="0" class="active"></li>
                <li data-target="#testimonials" data-slide-to="1"></li>
                <li data-target="#testimonials" data-slide-to="2"></li>
            </ol>
        
            <div class="carousel-inner">
                <div class="item active">
                    <blockquote>
                        <p>Waheguru Jaap is the religious website which has been started with purpose of religious awareness. Today, all of us don't have much time to do thanks to god for his kindness. We always waste our most of time on un-wanted things. With the help of this site we will keep trying to keep in your mind, the name of Waheguru. We will also give some gifts to the top Waheguru Naam Jaap user. We wish to god to fulfill your all dreams & take your all sorrows.
                        <small>Tarundeep Singh</small>
                        </p>
                        
                    </blockquote>
                </div>

            </div>
        </div> <!-- end carousel -->
</div>
</div>
<!--####################################################### End tart bg-testi-->

<div class="clear"></div>


<!--Start Container-->
<div class="container">





<!--Start Social Box-->
<div class="social_box">

<div class="coloumee">
<div class="left_soc">
<div class="round">1</div>
<h3>Like us on Facebook</h3>
</div>
<div class="clear"></div>
<iframe src="http://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fwahegurujaapji&amp;width=300&amp;height=350&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true&amp;appId=551588514939840" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:300px; height:350px;" allowTransparency="true"></iframe>
</div>


<div class="coloumee">
<div class="left_soc">
<div class="round">2</div>
<h3>Follow us on Twitter</h3>
</div>
<div class="clear"></div>
<a class="twitter-timeline" href="https://twitter.com/wahegurujaap" data-widget-id="492616648809926656">Tweets by @wahegurujaap</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

</div>


<div class="coloumee">
<div class="left_soc">
<div class="round">3</div>
<h3>Follow us on Google +</h3>
</div>
<div class="clear"></div>
<!-- Place this tag where you want the widget to render. -->
<div class="g-page" data-width="300" data-href="//plus.google.com/u/0/111269618238141460562" data-rel="publisher"></div>

<!-- Place this tag after the last widget tag. -->
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'http://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
</div>

</div>
<!--End Social Box-->
</div>
@endsection