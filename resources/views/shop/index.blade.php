@extends ('layouts.master')

@section ('content')
<div class="yellow-bar">
	<div class="left"></div>
	<div class="right"></div>
</div>
<div class="shop-wrapper">
@if(count($products))
	@foreach($products as $product)
	  <div class="col-md-4">
	    <div class="thumbnail">
	      <img src="{{ $product->image }}" alt="...">
	      <div class="caption">
	        <h3>{{ $product->title }}</h3>
	        <p><a href="{{ url('shop/add-to-cart') }}/{{ $product->id }}" class="btn btn-primary" role="button">Add to cart</a>
	        <span class="success">${{ $product->price }}</span>
	      </div>
	    </div>
	  </div>
	@endforeach
@endif
</div>
<style type="text/css">
	.shop-wrapper {
		float: left;
		width: 100%;		
	}
	.shop-wrapper .col-md-4 {
	    width: 31.5%;
	    float: left;
	    padding: 10px;
	}
</style>
@endsection