<?php if(!count(Cart::content())) die('Cart is empty'); ?>
<table>
   	<thead>
       	<tr>
           	<th>Product</th>
           	<th>Qty</th>
           	<th>Price</th>
           	<th>Subtotal</th>
            <th>Delete</th>
       	</tr>
   	</thead>

   	<tbody>

   		<?php $price=0; foreach(Cart::content() as $row) :?>

       		<tr>
           		<td>
               		<p><strong><?php echo $row->name; ?></strong></p>
               		<p><?php echo ($row->options->has('size') ? $row->options->size : ''); ?></p>
           		</td>
           		<td>
              <form action="{{ url('/shop/update-cart') }}/{{ $row->rowId }}" method="post">
              {{ csrf_field() }}
                <input type="text" name="qty" value="<?php echo $row->qty; ?>">               
                <input type="submit" value="update">               
              </form>
              </td>
           		<td><?php echo $row->price; ?></td>
             <?php //$price+=$row->price; ?> 
           		<td>$<?php echo $row->total; ?></td>
              <td><a class="button" href="{{ url('cart/destroy') }}/<?php echo $row->rowId; ?>">delete</a></td>
       		</tr>

	   	<?php endforeach;?>

   	</tbody>
   	
   	<tfoot>
   		<tr>
   			<td colspan="2">&nbsp;</td>
   			<td>Subtotal</td>
   			<td><?php echo Cart::subtotal(); ?></td>
   		</tr>
   		<tr>
   			<td colspan="2">&nbsp;</td>
   			<td>Tax</td>
   			<td><?php echo Cart::tax(); ?></td>
   		</tr>
   		<tr>
   			<td colspan="2">&nbsp;</td>
   			<td>Total</td>
   			<td><?php echo Cart::total(); ?></td>
   		</tr>
   	</tfoot>
</table>