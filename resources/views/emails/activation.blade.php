<!DOCTYPE html>
<html>
<head>
	<title>Activate you account</title>
</head>
<body>
<h2>Welcome {{ $data->firstname }} to wahegurujaap.com</h2>
<p>here is your account activation link: <a href="{{ url('/activate/' . $data->token ) }}">{{ url('/activate/' . $data->token ) }}</a></p>
</body>	
</html>