@extends ('layouts.master')

@section ('content')
	<div class="yellow-bar">
<div class="left"></div>
<div class="right"></div>
</div>
<!--End Yellow-bar-->
<!--Start Left Box-->
<div class="left-box">
<h2>{{ $video->title }}</h2>
<div class="clear"></div>
@if(!empty($video->video_link))
<iframe width="800" height="500" src="{{ $video->video_link }}" frameborder="0" allowfullscreen=""></iframe>
@else
<p>No video found</p>
@endif
<fieldset class="infoo">
  <legend><h3>Description</h3></legend>
<p>{{ $video->description }}</p>
</fieldset>
</div>
<!--End Left Box-->
<div class="bar">
<h3>Gallery</h3>
<h2>Total Videos : <span>( {{ count($videos) }} )</span></h2>
</div>
<!----End bar---->
<!--######################### thumbnail-->
<style>
.well{width:96.3%;background:none;border:0;}
.span3{background:none;}
.span3 h3 {
text-align: center;
font-size: 16px;
padding: 0;
margin: 0;
color: #3A3A3A;
}
.dis {
line-height: 18px;
font-size: 11px;
margin: 0;
padding: 0;
float: left;
text-align: center;
}
</style>
<div class="video-gallery">  
        
    <div class="well">
     
    <div id="myCarousel" class="carousel slide">
     
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
     
    <!-- Carousel items -->
    <div class="carousel-inner">
        
	<div class="item">
        <div class="row-fluid">
            @if(count($videos))
                @foreach($videos as $video)
                    <div class="span3">
                    <a href="{{ url('/video/' . $video->id) }}" class="thumbnail"><img src="{{ asset('/assets/gallery/' . $video->image) }}" width="100%" height="100%" alt=""/></a><h3>{{ $video->title }}</h3>
                    <p class="dis"></p>
                    </div>
                @endforeach
            @endif
        </div>
    </div>

   <!--       
        </div><!--/row-fluid-->
<!--    </div><!--/item-->
     
    </div><!--/carousel-inner-->
     
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
    </div><!--/myCarousel-->
     
    </div><!--/well-->
    
 </div>   
<div class="timelinee" id="review">
<div class="bar">
<h3>Enter Your Comments</h3>
<h2>Total Reviews : <span>( {{ count($video->comments) }} )</span></h2>
</div>
<div class="left-box">
<!--Start boxx here-->
<div class="boxx">
<form action="{{ url('/video/' . $video->id . '/comment') }}" method="post" accept-charset="utf-8">
{{ csrf_field() }}
<textarea name="review" id="review" class="jaap-field" placeholder="Enter Your Comments" ></textarea>
<button name="submit" type="submit" class="btn btn-info" id="my-button2">Submit</button></form></div>
<!--End boxx here-->
</div>
<div class="clear"></div>
    <div class="qa-message-list" id="wallmessages">
    @foreach($video->comments as $comment)
<div class="message-item" id="m16">
    <div class="message-inner">
        <div class="message-head clearfix">
            <div class="avatar pull-left"><a href="{{ url('/user/' . $comment->user->username)  }}"><img src="http://wahegurujaap.com/resources/front/images/profile_default.jpg" alt=""></a></div>
            <div class="user-detail">
                <h5 class="handle">{{ $comment->user->firstname }}</h5>
                <div class="post-meta">
                    <div class="asker-meta">
                        <span class="qa-message-what"></span>
                        <span class="qa-message-when">
                            <span class="qa-message-when-data">{{ $comment->created_at->format('F d, Y') }}</span>
                        </span>
                        <span class="qa-message-who">
                            <span class="qa-message-who-pad">by </span>
                            <span class="qa-message-who-data"><a href="{{ url('/user/' . $comment->user->username)  }}">{{ $comment->user->firstname }}</a></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="qa-message-content">{{ $comment->body }}</div>
</div>
</div>
@endforeach
    
</div>
</div>

@endsection