@extends ('layouts.master')

@section ('content')
<h2>Ragi Videos</h2>
<div class="clear"></div>
<!--Start Left Box-->
<div class="left-box">
@if(count($videos)>0)
	@foreach($videos as $video)
	<div class="main_gallery"><a href="{{ url('/video/' . $video->id) }}"><img src="{{ url('/assets	/gallery/' . $video->image) }}" class="photo_gallery" alt=""/></a><a href="{{ url('/video/' . $video->id) }}"><span>{{ $video->title }}</span></a></div>
	@endforeach
	@else
	<p>No item found.</p>
@endif
</div>

<div class="right-box">
<div class="title">Gurdwaras</div>
<ul class="nav nav-list">
	@if(count($gurudwaras))
		@foreach($gurudwaras as $gurudwara)
			<li><a href="{{ url('/gurudwara/' . $gurudwara->id) }}">{{ $gurudwara->name }}</a></li>
		@endforeach
	@endif
</ul>
</div></div>
@endsection