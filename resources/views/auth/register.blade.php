@extends('layouts.master')

@section('content')
<div class="yellow-bar">
<div class="left"></div>
<div class="right"></div>
</div>
<!--End Yellow-bar-->
<!--####################################################### Start bg-testi-->
<div class="bg_signup">
    <!--Start Form-->
<div id="main">
<div class="form_title">
<h2>Create an Account</h2>
<p>Join Wahegurujaap today and unlimited get rewards </p>
</div>
<form id="myform" method="POST" action="{{ route('register') }}">
    {{ csrf_field() }}
<div class="textfield">
    @if(Session::has('message')) <div class="alert alert-success"> {{Session::get('message')}} </div> @endif

    @if ($errors->has('firstname'))
        <span class="help-block">
            <strong>{{ $errors->first('firstname') }}</strong>
        </span>
    @endif
   <label>First Name</label><input type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" placeholder="Enter Your First Name"> </div>
 <div class="textfield">
  <label>Last Name</label><input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" placeholder="Enter Your Last Name"></div>
 <div class="textfield">
    @if ($errors->has('email'))
        <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
    @endif
  <label>Email</label><input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Enter Your Email"></div>
 <div class="textfield">
    @if ($errors->has('username'))
        <span class="help-block">
            <strong>{{ $errors->first('username') }}</strong>
        </span>
     @endif
   <label>Username</label><input type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="Enter Your User Name"> </div>
  <div class="textfield">
    @if ($errors->has('password'))
        <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
    @endif
   <label>Password</label> <input type="password" name="password" value="" placeholder="Enter a password" id="pass" /> </div>
 <div class="textfield">
 <label>Confirm Password</label>    <input type="password" name="password_confirmation" value="" placeholder="Confirm Your password" /> </div>
 <div class="textfield">
   <label>Mobile</label> <input type="text" name="mobile" value="{{ old('mobile') }}" placeholder="Choose Your Contact no" /> </div>  
 <div class="textfield">
   <label>&nbsp;</label>
   <input type="submit" style="display:none" />
   
    <button name="mybutton" type="submit" class="btn btn-warning" id="mybutton">Signup Now!</button>  
</form> 
<ul>
@foreach($errors as $error)
    <li>{{ $error }}</li>
@endforeach
</ul>
</div>
<div class="clear"></div>
</div>
         
<!--End form-->
<!--Start Social-->
<div class="social">
<h1>Signup with Socials </h1>
<div class="social_btn">

<a href="../facebook.html" class="btn btn-block btn-social btn-facebook" style="background:#3b5998"><i class="fa fa-facebook"></i>Sign Up with Facebook</a></div>
<div class="social_btn">
<a href="https://accounts.google.com/o/oauth2/auth?client_id=555188097511-3qt6kkmpn8sddp19e018t3lbh3e7o2fn.apps.googleusercontent.com&amp;redirect_uri=http%3A%2F%2Fwahegurujaap.com%2Foauth%2Flogin%2Fgoogle&amp;state=0f50ed5254ca65e70009f4670351fd8f&amp;scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&amp;response_type=code&amp;approval_prompt=force" class="btn btn-block btn-social btn-google-plus" style="background:#ca3523"><i class="fa fa-google-plus"></i>Sign Up with Google</a></div>
<div class="social_btn">
<a href="https://api.twitter.com/oauth/authenticate?oauth_token=4TqZCQAAAAAA0uHTAAABXOscT08" class="btn btn-block btn-social btn-twitter" style="background:#55acee"><i class="fa fa-google-plus"></i>Sign Up with Twitter</a></div>
</div>
<!--End Social-->
</div>
@endsection
