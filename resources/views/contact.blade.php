@extends ('layouts.master')
@section ('title')
Contact
@endsection
@section ('content')
<div class="yellow-bar">
<div class="left"></div>
<div class="right"></div>
</div>
<div class="bg_signup">

	<!--Start Form-->
<div id="main">
<div class="form_title">
<h2>Contact Us</h2>
<p>Join Wahegurujaap today and unlimited get rewards </p>
</div>
@if(Session::has('message')) <div class="alert alert-success"> {{Session::get('message')}} </div> @endif
@if(count($errors))
<ul class="alert alert-danger" style="list-style-type: none;">
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
@endif
<form action="{{ url('contact/send') }}" id="myform" method="post" accept-charset="utf-8">  <div class="textfield">
  {{ csrf_field() }}
    <label>Full Name</label>    
    <input type="text" name="fullname" value="" placeholder="Enter Your Full Name">  </div>
  <div class="textfield">
   <label>Email</label>     <input type="text" name="email" value="" placeholder="Enter Your Email address">  </div>
  <div class="textfield">
   <label>Message</label>     <textarea name="message" placeholder="Type Your Message" rows="5" cols="30"></textarea>  </div>

  <div class="textfield">
    <label>&nbsp;</label>
    <input type="submit" style="display:none">
    
     <button name="mybutton" type="submit" class="btn btn-warning" id="mybutton">Submit</button>     </div></form>

<div class="clear"></div>
</div>
          
<!--End form-->


</div>
@endsection