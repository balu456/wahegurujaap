@extends ('layouts.master')

@section ('content')
<div class="clear"></div>
<div class="yellow-bar">
<div class="left"></div>
<div class="right"></div>
</div>
<!--End Yellow-bar-->
<!--Start Left Box-->
<div class="left-box">
<h2><span>{{ $ragi->name }}</span></h2>
<!--Start boxx here-->
<div class="boxx">
<div class="bar">
<h3>Star Rating  &nbsp; <i class="fa fa-star"></i>  <i class="fa fa-star"></i>  <i class="fa fa-star"></i>  <i class="fa fa-star"></i>  <i class="fa fa-star fa-star-half-empty"></i> </h3>
<h2>Reviews: <span>{{ count($ragi->comments) }}</span></h2>
</div>
<img src="{{ asset('/assets/gallery/' . $ragi->image) }}" class="img" width="205" height="210" alt=""/> <fieldset class="infoo">
  <legend><h3>Basic Information</h3></legend>
  <p>{{ $ragi->description }}</p>
 </fieldset>
</div>
<!--End boxx here-->
</div>
<!--End Left Box-->


<!--Start Right Box-->
<div class="right-box">
<!--Start title-->
<div class="title">
Gurudwaras List
</div>
<!--End title-->
    <div class="users" id="gurudwara">
    @if(count($gurudwaras))
    @foreach($gurudwaras as $gurudwara)
    <a href="{{ url('/gurudwara' . $gurudwara->id) }}"><img src="{{ asset('/assets/gallery/' . $gurudwara->image) }}" class="img-rounded" width="79" alt=""/></a>    <p><a href="{{ url('/gurudwara/' . $gurudwara->id) }}">{{ $gurudwara->name }}</a></p>
    @endforeach
        @else
        <p>No item found.</p>
    @endif
    </div>
<div class="view_more">
<a href="{{ url('gurudwaras') }}">See All</a></div>
</div>
<!--End Right Box-->



<!----start bar---->
<div class="bar">
<h3>Videos</h3>
<h2>Total Videos : <span>( {{ count($ragi->videos) }} )</span></h2>
</div>
<!----End bar---->
<!--######################### thumbnail-->
<style>
.well{width:96.3%;background:none;border:0;}
.span3{background:none;}
.span3 h3 {
text-align: center;
font-size: 18px;
padding: 0;
margin: 0;
color: #3A3A3A;
}
.dis {
line-height: 18px;
font-size: 13px;
margin: 0;
padding: 0;
float: left;
text-align: center;
}
</style>
<div class="video-gallery">  
        
    <div class="well">
     
    <div id="myCarousel" class="carousel slide">
     
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
     
    <!-- Carousel items -->
    <div class="carousel-inner">
                @if(count($ragi->videos))
                @foreach($ragi->videos as $video)    
    <div class="item">
        <div class="row-fluid">

                    <div class="span3">
                    <a href="{{ url('/video/' . $video->id) }}" class="thumbnail"><img src="{{ asset('/assets/gallery/' . $video->image) }}" width="100%" height="100%" alt=""/></a><h3>{{ $video->title }}</h3>
                    <p class="dis"></p>
 
                </div>
            </div>
        </div>
                       @endforeach
            @endif
    </div><!--/carousel-inner-->
     
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
    </div><!--/myCarousel-->
     
    </div><!--/well-->
    
 </div>   

<div class="timelinee" id="review">
<div class="bar">
<h3>Enter Your Comments</h3>
<h2>Total Reviews : <span>( {{ count($ragi->comments) }} )</span></h2>
</div>
<div class="left-box">
<!--Start boxx here-->
<div class="boxx">
<form action="{{ url('/ragi/' . $ragi->id . '/comment') }}" method="post" accept-charset="utf-8"><textarea name="review" id="review" class="jaap-field" placeholder="Enter Your Comments" ></textarea>
{{ csrf_field() }}
<button name="submit" type="submit" class="btn btn-info" id="my-button2">Submit</button></form></div>
<!--End boxx here-->
</div>
<div class="clear"></div>
    <div class="qa-message-list" id="wallmessages">
    @foreach($ragi->comments as $comment)
<div class="message-item" id="m16">
    <div class="message-inner">
        <div class="message-head clearfix">
            <div class="avatar pull-left"><a href="{{ url('/user/' . $comment->user->username)  }}"><img src="http://wahegurujaap.com/resources/front/images/profile_default.jpg" alt=""></a></div>
            <div class="user-detail">
                <h5 class="handle">{{ $comment ->user->firstname }}</h5>
                <div class="post-meta">
                    <div class="asker-meta">
                        <span class="qa-message-what"></span>
                        <span class="qa-message-when">
                            <span class="qa-message-when-data">{{ $comment ->created_at->format('F d, Y') }}</span>
                        </span>
                        <span class="qa-message-who">
                            <span class="qa-message-who-pad">by </span>
                            <span class="qa-message-who-data"><a href="{{ url('/user/' . $comment->user->username)  }}">{{ $comment ->user->firstname }}</a></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="qa-message-content">{{ $comment ->body }}</div>
</div>
</div>
@endforeach
    
</div>
</div>
@endsection