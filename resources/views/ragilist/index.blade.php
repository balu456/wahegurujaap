@extends ('layouts.master')

@section ('content')
	<h2>Ragi List</h2>
<div class="clear"></div>
<div class="left-box">

@if(count($ragis)>0)
@foreach($ragis as $ragi)
<div class="ragi_list">
<a href=".{{ url('/ragi/' . $ragi->id) }}"><img src="{{ asset('/assets/gallery/' . $ragi->image) }}" class="img-rounded" width="80" height="60" id="ragi_icon" alt=""/></a><table border="0">
<tr>
<td>
<h3><a href="{{ url('/ragi/' . $ragi->id) }}">{{ $ragi->name }}</a></h3>
<div class="clear"></div>
<p>
{{ $ragi->description }}</p>
</td>
<td>
<h3 style="text-align:center;float:none;margin: -15px 0px 10px 0px;">Videos</h3>
<p style="width:80px; float:right;">
<span><i class="fa fa-video-camera"></i>{{ count($ragi->videos) }}</span> 
</p>
</td>
</tr>
</table>
<div class="bar">
<h3>Your Rating  &nbsp; <i class="fa fa-star"></i>  <i class="fa fa-star"></i>  <i class="fa fa-star"></i>  <i class="fa fa-star"></i>  <i class="fa fa-star fa-star-half-empty"></i> </h3>
<h2>Reviews : <span>{{ count($ragi->comments) }}</span></h2>
</div>
</div>
@endforeach

@else
	<p>No item found.</p>
@endif


</div>
@endsection