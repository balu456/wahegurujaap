@extends ('layouts.master')

@section ('content')
<div class="left-box">
<h2>Winner List</h2>
<div class="boxx">
<div class="bar">
<h3>Winner List</h3>
<h2>Total Jaaps : <span>( {{ $jaaps }} )</span></h2>
</div>
<div class="panel panel-default">
    
      <!-- Table -->
      <table class="table" style="float:left;">
        <thead>
          <tr>
            <th>Winner Image</th>
            <th>Winner Name</th>
            <th>From</th>
            <th>To</th>
            <th>Total Jaap</th>
          </tr>
        </thead>
        <tbody>
    @if($winners)
      @foreach($winners as $winner)
          <tr>
          <td><img src="{{ asset('wahegurujaap//resources/front/images/profile_default.jpg') }}" class="img-polaroid" width="50" alt=""></td>
            <td>{{ $winner->firstname }}</td>
            <td>July 27, 2014</td>
            <td>July 27, 2014</td>
            <td>{{ $winner->jaap }}</td>
          </tr>
      @endforeach
    @endif

        </tbody>
      </table>
    </div>
    
    
<div class="clear"></div>
</div>
</div>
@endsection