<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!-- Mirrored from wahegurujaap.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 27 Jun 2017 19:49:34 GMT -->
<head>
 <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- metadata starts -->


<meta name="google-site-verification" content="l4llWR0TofS1_OlmRbBuY3M69FeooqWlsbYsVKwXyiA" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<!-- metadata ends -->
<title>Wahegurujaap - @yield ('title')</title>
<!-- css starts -->
<!-- Stylesheets -->
<link rel="stylesheet" href="{{ asset('wahegurujaap/css/bootstrap.css') }}" />
<link rel="stylesheet" href="{{ asset('wahegurujaap/css/bootstrap-responsive.css') }}" />
<link rel="stylesheet" href="{{ asset('wahegurujaap/css/custom.css') }}" />
<link rel="stylesheet" href="{{ asset('wahegurujaap/css/style.css') }}"/>
<link href="{{ asset('wahegurujaap/css/bootstrap-social.css') }}" rel="stylesheet" >
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="wahegurujaap/fonts/css/font-awesome.css">
<link rel="stylesheet" href="{{ asset('wahegurujaap/fonts/css/font-awesome.min.css') }}">
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,200,700,800,900' rel='stylesheet' type='text/css'>

<!-- javascript starts -->
<script src="{{ asset('wahegurujaap/js/jquery-1.9.1.min.js') }}"></script>
<script src="{{ asset('wahegurujaap/js/ajax.js') }}"></script>
<script>
function waheguru_record(){
    var audioElement = document.createElement('audio');
    audioElement.setAttribute('src', "{{ asset('wahegurujaap/resources/front/waheguru.mp3') }}");
    audioElement.setAttribute('autoplay', 'autoplay');
    //audioElement.load()
    
    /*$.get();*/
    
    audioElement.addEventListener("load", function() {
        audioElement.play();
    }, true);
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
    }
    return "";
} 

$(document).ready(function(){
            $('#loading').hide();
            $("#jaaparea").val("");
            
            $("#jaap").keydown(function(event) {
                 if (((event.keyCode == 87 || event.keyCode == 65||event.keyCode == 72|| event.keyCode == 69||event.keyCode==71 ||event.keyCode==85||event.keyCode ==82||event.keyCode ==8||event.keyCode ==101||event.keyCode ==32) ) == false) 
                 {
                    event.preventDefault(); 
                                
                }
            });
            
             $('#jaap').bind('copy paste delete drop', function(e) {
                e.preventDefault();
                });
   
            $('form').submit(function()
        {
        $('#loading').show();
        $('.buttonSubmit').hide();
        });
            });
            $(document).ready(function() {
    $('#jaap').keyup (function() {
         var as = $(this).val().match(/waheguru/ig) || [];
         var currentdate = new Date(); 
         
         
         var jaap=parseInt($('.jaap').html()) + 1;
         $(':input[type="submit"]').prop('disabled', false);
         if (jaap==as.length){
             x=as.length;
             document.cookie='r_jaaps=' + x;
             
             
            var stime=$('#stime').html();
            if (stime==currentdate.getSeconds()){
                $(this).val('');
            }
            waheguru_record();
            $('#stime').html(currentdate.getSeconds());
        }
        
         count=0;
         if((as.length % 108) == 0 ) 
         {
         $('.mala').text(as.length/108);
         /*$('.smala').val(as.length/108);*/
         }
         else
         {
            if(as.length<108)
            {
          $('.mala').text(0);
           /*$('.smala').val(0);*/
           }
         }
        $('.jaap').text(as.length);
         $('.scount').val(as.length);
    });
});
</script><!-- javascript ends -->
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "../connect.facebook.net/en_US/sdk.js#xfbml=1&appId=551588514939840&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<!--Start full_blue-bar here-->
<div class="full_blue-bar"></div>
<!--Start container here-->
<div class="container">
    @include ('layouts.header')
    @yield ('content')
    <!--End container here--></div>
<style>.control-label { text-align:center !important;}</style>
<div id="stime" style=" display:none;">0</div>
<div class="modal hide fade" id="my-modal">
<div class="modal-header">
    <button class="close" data-dismiss="modal">&times;</button>
    <h2 style="float:none;"><span>Login</span> Here</h2>
</div>
<div class="modal-body">
<p class="alert alert-success" id="success" style="display: none"></p>
<p class="alert alert-warning" id="error" style="display: none"></p>
<form id="login-form" action="{{ url('userlogin') }}" method="POST">
{{ csrf_field() }}
<div id="form_error"></div>
<div class="control-group form-horizontal">
    <label class="control-label">Username</label>
    <div class="controls"><input type="text" name="username" value="" id="email" placeholder="Username"  /></div>            
</div>
<div class="control-group form-horizontal">
    <label class="control-label">Password</label>
    <div class="controls"><input type="password" name="password" value="" id="password" placeholder="Password"  /></div>            
</div>
<div class="control-group form-horizontal">
    <div class="controls">
    <label class="control-label"></label>
        <input type="submit" name="button" value="Submit" class="btn btn-warning" id="my_login" />        <!-- <div id="login_load"></div> -->
    </div>
</div>
<div class="control-group form-horizontal">
    <label class="control-label"></label>
    <div class="controls"><a href="#">Forgot your Password?</a> | <a href="{{ url('/activate') }}">Got Your Activation Link?</a></div>            
</div>
</form>
</div>
    @include ('layouts.footer')
<script src="{{ asset('wahegurujaap/js/bootstrap.js') }}"></script>
<script src="wahegurujaap/js/jquery.validate.min.html"></script>
<script type="text/javascript">
    $("#my-button").click(function(){
        $("#my-modal").modal();
    });
</script>
@if (Auth::guest())
<script type="text/javascript">
    $("#my-button2").click(function(e){
        e.preventDefault();
        $("#my-modal").modal();
    });
</script>
@endif
<script type="text/javascript">
$(document).ready(function() {
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#login-form").on('submit', function(e){
            e.preventDefault();
            console.log('detected');
        $.ajax({
            type: "POST",
            url:  "{{   url('userlogin') }}",
            data: $(this).serializeArray(),
            contentType: $(this).data('type'),
            success: function (data) {
                msg = JSON.parse(data);
                if(msg.message=='success') {
                    $('#error').css('display', 'none');                                                        
                    $('#success').css('display', 'block');                                      
                    $('#success').html('loging in..');
                      location.reload(); 
                        } else {
                  $('#error').css('display', 'block');                          
                    $('#error').html(msg.message);                  
                }
            }
        });
     });
});
</script>
</body>

<!-- Mirrored from wahegurujaap.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 27 Jun 2017 19:50:26 GMT -->
</html>