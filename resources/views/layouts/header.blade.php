<div class="logo_box">
<!--Start logo-->
<a href="{{ url('/') }}"><div class="logo"></div></a><!--End logo-->
    <!--Start Email-phone-->
    <div class="email_phone">
    <ul>
    <li><i class="fa fa-envelope"></i><span>info@wahegurujaap.com</span></li>
    <li style="border:0;">
    <button class="btn btn-warning" style="color: #000;text-shadow: none;font-weight: bold;" >Total Jaaps : {{ $jaaps }}                                            </button></li>
    </ul>
        <!--Start Search-->
        <div class="search-box">
        <input type="text" class="search" style="background-color: #f3f3f3;">
        </div>
        <!--End Search-->
    </div>
    <!--end Email-phone-->

<div class="menunavi">
<div class="navbar">
<div class="navbar-inner">
<a data-toggle="collapse" data-target=".nav-collapse" class="btn btn-navbar">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</a>
<div class="nav-collapse colapse">
<ul class="nav pull-left">
<li {{ (Request::is('/') ? 'class=active' : '') }}><a href="{{ url('/') }}">Home</a></li>
<li {{ (Request::is('gallery') ? 'class=active' : '') }}><a href="{{ url('/gallery') }}">Photo Gallery</a></li>
<li {{ (Request::is('videos') ? 'class=active' : '') }}><a href="{{ url('/videos') }}">Videos</a></li>
<li {{ (Request::is('kirtan') ? 'class=active' : '') }}><a href="{{ url('kirtan') }}">Live Kirtan</a></li>
<li {{ (Request::is('winners') ? 'class=active' : '') }}><a href="{{ url('winners') }}">Winner List</a></li>
<li {{ (Request::is('ragis') ? 'class=active' : '') }}><a href="{{ url('ragis') }}">Ragi List</a></li>
<li {{ (Request::is('gurudwaras') ? 'class=active' : '') }}><a href="{{ url('gurudwaras') }}">Gurdwara List</a></li>
<li {{ (Request::is('users') ? 'class=active' : '') }}><a href="{{ url('users') }}">Users List</a></li>
<li {{ (Request::is('contact') ? 'class=active' : '') }}><a href="{{ url('contact') }}">Contact</a></li>
<li {{ (Request::is('shop') ? 'class=active' : '') }}><a href="{{ url('shop') }}">Shop</a></li>
</ul>
<style>
.navbar .nav > li > a:focus, .navbar .pull-right > li > a:hover {
color: #575757;
background:none;
}
</style>
<ul class="nav pull-right">
 <!-- Authentication Links -->
<li id="cart"><a href="{{ url('/cart') }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart {{ Cart::count() }}</a></li>
                    @if (Auth::guest())
                    <li id="sign_profile"><a href="#my-modal" id="my-button"><i class="fa fa-sign-in"></i>  Login</a></li>
                    <li id="sign_logout"><a href="{{ url('/register') }}"><i class="fa fa-user"></i>  Signup</a></li>
                     <!--    <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li> -->
                    @else
                    <li id="sign_profile"><a href="{{ url('/profile/' . Auth::user()->username ) }}" id=""><i class="fa fa-sign-in"></i> Profile</a></li>
                       <li id="sign_logout"><a href="{{ url('/logout') }}"><i class="fa fa-user"></i> Logout</a></li>
                    @endif
</ul>
</div>
</div>
</div>
</div>
        <div class="clear"></div>
</div>