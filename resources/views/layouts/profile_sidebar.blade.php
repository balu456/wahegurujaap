<div class="right-box">
<!--Start title-->
<div class="title">
 User Profile
</div>
<!--End title-->
<div class="imageContainer">
@if(! Auth::user()->image)
<img src="http://wahegurujaap.com/resources/front/images/profile_default.jpg" width="100" height="100" id="profileImage" class="img-rounded" alt="{{ Auth::user()->username }}"/>
@else
<img src="{{ asset('assets/members/' . Auth::user()->image) }}" width="100" height="100" id="profileImage" class="img-rounded" alt="{{ Auth::user()->username }}"/>
@endif
</div>
<h3>{{ Auth::user()->firstname }}</h3>
<h4>Today Jaap : <span>0</span></h4>
<h4>Total Jaap : <span>{{ Auth::user()->jaap }}</span></h4>
<h4>Total Mala : <span>0</span></h4>
<div class="clear"></div>
<ul class="nav nav-list">
	<li>
    <a href="{{ url('/profile/' . Auth::user()->username ) }}"><i class="fa fa-user"></i><span class="menu-text">My Profile</span></a>    </li>
    <li>
    <a href="{{ url('/profile/' . Auth::user()->username ) }}/edit"><i class="fa fa-user"></i><span class="menu-text">Edit Profile</span></a>    </li>
    <li>
    <a href="{{ url('/profile/' . Auth::user()->username ) }}/reset"><i class="fa fa-lock"></i><span class="menu-text">Change Password</span></a>    </li>
    <li>
    <a href="{{ url('/profile/' . Auth::user()->username ) }}/comments"><i class="fa fa-comments"></i><span class="menu-text">View Comments</span></a>    </li>
    <li>
    <a href="{{ url('/profile/' . Auth::user()->username ) }}/jaap"><i class="fa fa-file-text"></i><span class="menu-text">Start Jaap</span></a>    </li>
    <li>
    <a href="{{ url('/profile/' . Auth::user()->username ) }}/jaaps"><i class="fa fa-file-text"></i><span class="menu-text">My Jaap Record</span></a>    </li>
    <li>
    <a href="{{ url('/logout') }}"><i class="fa fa-sign-out"></i><span class="menu-text">Log Out</span></a>    </li>
</ul>
</div>