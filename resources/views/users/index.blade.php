@extends ('layouts.master')

@section ('content')
<div class="yellow-bar">
<div class="left"></div>
<div class="right"></div>
</div>
<!--End Yellow-bar-->
<h2>Users List</h2>
<div class="clear"></div>
<div class="left-box">

@if(count($users)>0)
@foreach($users as $user)
<div class="ragi_list">
	<a href="{{ url('/user') }}/{{ $user->username }}"><img src="{{ asset('/assets/members/' . $user->image) }}" class="img-rounded" width="80" height="60" id="ragi_icon" alt=""/></a><table border="0">
	<tr>
	<td style="width:78%">
	<h3><a href="{{ url('/user') }}/{{ $user->username }}">{{ $user->firstname }}</a></h3>
	<div class="clear"></div>
	@if(!$user->about)
	<p>User does not mention about me.</p>
	@else
	<p>{{ $user->about }}</p>
	@endif
	</td>
	<td style="width:22%">
	<h3 style="float: left; margin: -15px 0 10px; text-align: center; width: 100%;">Jaap</h3>
	<p style="float: left; width: 100%;">
	<span style="width: 100%; float: right;"><i class="fa fa-bar-chart-o"></i> {{ $user->jaap }} </span> 
	</p>
	</td>
	</tr>
	</table>
	<div class="bar">
	<h3>Registered On : {{ $user->created_at->format('F d, Y') }}</h3>
	<h2>Winner of week: <span>(0 Times)</span></h2>
	</div>
</div>
@endforeach
@else 
 No user found.
@endif
</div>
@endsection