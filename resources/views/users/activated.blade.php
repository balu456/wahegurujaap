@extends ('layouts.master')

@section('content')
	<div class="yellow-bar">
<div class="left"></div>
<div class="right"></div>
</div>
<!--End Yellow-bar-->
<!--####################################################### Start bg-testi-->
<div class="bg_signup">
	<!--Start Form-->
<div id="main">
<p>@if(Session::has('message')) <div class="alert alert-success"> {{Session::get('message')}} </div> @endif</p>
</div>
</div>
@endsection