@extends ('layouts.master')

@section('content')
<script src="//geodata.solutions/includes/countrystatecity.js"></script>
<div class="yellow-bar">
<div class="left"></div>
<div class="right"></div>
</div>
<div class="left-box">
<h2>Edit Profile</h2>
<div class="boxx">
<div class="bar">
<h3> </h3>
<h2>Total Votes : <span>( 550 )</span></h2>
</div>
<fieldset class="infoo" style="max-width: 100%; width: 93%;">
<legend><h3>Edit Your Profile</h3></legend>
@if(Session::has('message')) <div class="alert alert-info"> {{Session::get('message')}} </div> @endif
@if(count($errors))
<ul class="alert alert-danger" style="list-style-type: none;">
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
@endif
<form action="update" method="post" accept-charset="utf-8"><ul>
{{ csrf_field() }}
<li><span>First Name :</span> <font><input type="text" name="firstname" value="{{ $user->firstname }}" id="firstname" placeholder="First Name"></font></li>

<li><span>Last Name :</span> <font><input type="text" name="lastname" value="{{ $user->lastname }}" id="lastname" placeholder="Last Name"></font></li>

<li><span>DOB :</span> <font>
<select name="day" style="width:80px;">
<option value="">Day</option>

<?php for ($i=1; $i <= 31; $i++) { 

	echo "<option value='".$i."'>".$i."</option>";
 
 } ?>

</select><select name="month" style="width:80px;">
<option value="">Month</option>

<?php for ($i=1; $i <= 31; $i++) {

	echo "<option value='".$i."'>".$i."</option>";

} ?>
</select><select name="year" style="width:80px;">
<option value="">Year</option>
<?php for ($i=date('Y'); $i >=1950 ; $i--) { 

	echo "<option value='".$i."'>".$i."</option>";

} ?>

</select></font></li>

<li><span>Address :</span> <font><textarea name="address" id="address" placeholder="Address" rows="2">{{ $user->address }}</textarea></font></li>

<li><span>Country :</span><font>
	<select name="country" class="countries" id="countryId">
    <option value="">Select Country</option>
    @if($user->country)
        <option value="{{ $user->country }}" selected="">{{ $user->country }}</option>
    @endif
	</select>
</font></li>

<!-- <select name="city" class="cities" id="cityId">
    <option value="">Select City</option>
</select>
 -->
<li><span>State :</span><font><select name="state" class="states" id="stateId">
    <option value="">Select State</option>
    @if($user->state)
        <option value="{{ $user->state }}" selected="">{{ $user->state }}</option>
    @endif
</select></font></li>

<li><span>City :</span> <font><input type="text" name="city" value="{{ $user->city }}" id="city" placeholder="City"></font></li>

<li><span>Zip Code :</span> <font><input type="text" name="zipcode" value="{{ $user->zipcode }}" id="zip" placeholder="Zip Code"></font></li>

<li><span>Mobile :</span> <font><input type="text" name="mobile" value="{{ $user->mobile }}" id="mobile" placeholder="Mobile Number"></font></li>

<li><span>About Me:</span> <font><textarea name="about" id="about" placeholder="About Me" rows="5" cols="5">{{ $user->about }}</textarea></font></li>

<li><span>&nbsp;</span> <font>

<input type="submit" name="submit" value="Submit" class="btn btn-warning"></font></li>
</ul>
</form> </fieldset>
<div class="clear"></div>
</div>
</div>
@include('layouts.profile_sidebar')
@endsection
