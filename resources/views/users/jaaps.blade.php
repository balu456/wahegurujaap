@extends('layouts.master')

@section('content')
	<div class="yellow-bar">
<div class="left"></div>
<div class="right"></div>
</div>
<!--End Yellow-bar-->
<div class="left-box">
<h2>My Jaap Record</h2>
<div class="boxx">
<div class="bar">
<h3></h3>
<h2>Total Votes : <span>( 550 )</span></h2>
</div>
<div class="panel panel-default">
    
      <!-- Table -->
      <table class="table" style="float:left;">
        <thead>
          <tr>
            <th>#</th>
            <th>Date</th>
            <th>Time</th>
            <th>Total Jaap</th>
            <th>Total Mala</th>
          </tr>
        </thead>
        <tbody>
@if(count($user_jaaps))
<?php $i = 1; ?>
  @foreach($user_jaaps as $jaap)           
          <tr>
            <td><?php echo $i++; ?></td>
            <td>{{ $jaap->created_at->format('F d, Y')  }}</td>
            <td>{{ $jaap->created_at->format('H:i:s A') }}</td>
            <td>{{ $jaap->jaap }}</td>
            <td>0</td>
          </tr>
  @endforeach
@endif
        </tbody>
      </table>
    </div>
    
    
<div class="clear"></div>
</div>
</div><link rel="stylesheet" type="text/css" href="http://wahegurujaap.com/wahegurujaap/css/imgareaselect-animated.css" />
<script type="text/javascript" src="http://wahegurujaap.com/wahegurujaap/js/jQuery-custom-input-file.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="http://wahegurujaap.com/wahegurujaap/js/jquery.imgareaselect.pack.js"></script>
<script type="text/javascript" src="http://wahegurujaap.com/wahegurujaap/js/script.js"></script>
<!--Start Right Box-->
@include('layouts.profile_sidebar')
@endsection