@extends ('layouts.master')

@section ('content')
<div class="yellow-bar">
<div class="left"></div>
<div class="right"></div>
</div>
<!--End Yellow-bar-->
<!--Start Left Box-->
<div class="left-box">
<h2><span>Dhillon Harcharan singh</span> Profile</h2>
<!--Start boxx here-->
<div class="boxx">
<div class="bar">
<h3>Winner of week: <span>(0 Times)</span></h3>
<h2>Total Jaaps: <span>{{ $user->jaap }}</span></h2>
</div>
<img src="{{ asset('/assets/members/' . $user->image) }}" class="img" width="205" height="210" alt=""/> <fieldset class="infoo">
  <legend><h3>Basic Information</h3></legend>
  <p><b>Name : </b>{{ $user->firstname }}</p>
  <p><b>Username : </b>{{ $user->username }}</p>
  <p><b>DOB : </b>{{ $user->dob }}</p>
  <p><b>Loction : </b>{{ $user->city }}</p>
  <p><b>Zip Code : </b>{{ $user->zipcode }}</p>
  <p><b>Registered On : </b>{{ $user->created_at->format('F d, Y') }}</p>
  <p><b>About : </b>{{ $user->about }}</p>
 </fieldset>
</div>
<!--End boxx here-->
</div>
<!--End Left Box-->

</div>
@endsection