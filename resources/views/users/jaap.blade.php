@extends('layouts.master')

@section('content')
<div class="yellow-bar">
<div class="left"></div>
<div class="right"></div>
</div>
<div class="left-box">
@if(Session::has('message'))<div class="alert alert-success" role="alert"><i class="fa fa-check"></i>{{Session::get('message')}}</div> @endif


<h2>Start Jaap Today ! (Type <span>Waheguru</span> in the box)</h2>
<!--Start Buttons-->
<div class="buttons">
<div class="count">
<button class="btn btn-warning jaap" id="my_btn">0</button>
<p>Jaap</p>
</div>
<div class="count">
<button class="btn btn-warning mala" id="my_btn">0</button>
<p>Mala</p>
</div>
</div>
<!--End Buttons-->
<div class="vol">
<button class="btn play"><i class="fa fa-volume-up"></i></button>
<button class="btn pause"><i class="fa fa-volume-off"></i><i class="fa fa-times"></i></button>
</div>
<form action="{{ url('/jaap') }}" method="post" accept-charset="utf-8">
{{  csrf_field() }}
<textarea name="jaap" id="jaap" class="jaap-field" placeholder="Start Jaap Today"></textarea><div class="clear"></div>
<input type="submit" name="submit" value="Submit" class="btn btn-info"></form></div>

@include('layouts.profile_sidebar')
@endsection