@extends('layouts.master')

@section('content')
	<div class="yellow-bar">
<div class="left"></div>
<div class="right"></div>
</div>
<!--End Yellow-bar-->
<div class="left-box">
<h2>User Comments</h2>
<div class="boxx">
<div class="bar">
<h3></h3>
<h2>Total Reviews : <span>( {{ count($user->comments) }} )</span></h2>
</div>
<div class="panel panel-default">
    
      <!-- Table -->
      <table class="table" style="float:left;">
        <thead>
          <tr>
            <th>Review Message</th>
            <th>Review Date</th>
            <!-- <th>Review Status</th> -->
          </tr>
        </thead>
        <tbody>
        @if(count($user->comments))
	     	@foreach($user->comments as $comment)
	          <tr>
	            <td>{{ $comment->body }}</td>
	            <td>{{ $comment->created_at->format('F d, Y') }}</td>
	            <!-- <td>inactive</td> -->
	          </tr>
	        @endforeach
	        @else
	        <p>No comments found.</p>
        @endif
               </tbody>
      </table>
    </div>
    
    
<div class="clear"></div>
</div>
</div><link rel="stylesheet" type="text/css" href="http://wahegurujaap.com/wahegurujaap/css/imgareaselect-animated.css" />
<script type="text/javascript" src="http://wahegurujaap.com/wahegurujaap/js/jQuery-custom-input-file.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="http://wahegurujaap.com/wahegurujaap/js/jquery.imgareaselect.pack.js"></script>
<script type="text/javascript" src="http://wahegurujaap.com/wahegurujaap/js/script.js"></script>
<!--Start Right Box-->
@include('layouts.profile_sidebar')
@endsection