@extends ('layouts.master')

@section('content')
<div class="yellow-bar">
<div class="left"></div>
<div class="right"></div>
</div>
<div class="left-box">
<h2>Change Password</h2>
<div class="boxx">
<div class="bar">
<h3></h3>
<h2>Total Votes : <span>( 550 )</span></h2>
</div>
<fieldset class="infoo" style="max-width: 100%; width: 93%;">
<legend><h3>Change Password</h3></legend>
<form action="reset" method="post" accept-charset="utf-8">
{{ csrf_field() }}
<ul>
<li><span>Old Password</span> <font><input type="password" name="old_password" value="" id="old_password" placeholder="Your Old Password"></font></li>
<li><span>New Password</span> <font><input type="password" name="new_password" value="" id="new_password" placeholder="Your New Password"></font></li>
<li><span>Confirm Password</span> <font><input type="password" name="confirm_password" value="" id="confirm_password" placeholder="Confirm Your Password"></font></li>
<li><span>&nbsp;</span> <font>
<input type="submit" name="submit" value="Submit" class="btn btn-warning"></font></li>
</ul>
</form> </fieldset>
@if(Session::has('message')) <div class="alert alert-info"> {{Session::get('message')}} </div> @endif
@if(count($errors))
<ul class="alert alert-danger" style="list-style-type: none;">
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
@endif
<div class="clear"></div>
</div>
</div>
	@include('layouts.profile_sidebar')
@endsection