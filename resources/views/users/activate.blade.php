@extends ('layouts.master')

@section('content')
	<div class="yellow-bar">
<div class="left"></div>
<div class="right"></div>
</div>
<!--End Yellow-bar-->
<!--####################################################### Start bg-testi-->
<div class="bg_signup">
	<!--Start Form-->
<div id="main">
@if(Session::has('message')) <div class="alert alert-info"> {{Session::get('message')}} </div> @endif
<form action="{{ url('activate') }}" method="post">
	{{ csrf_field() }}
	<div class="form-group">
      <label for="activation">Activation link:</label>
      <input type="activation_link" class="form-control" id="email" placeholder="Activation link" name="activation_link">
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
</form>
</div>
</div>
@endsection