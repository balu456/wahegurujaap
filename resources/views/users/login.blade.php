@extends ('layouts.master')

@section('content')
	<div class="yellow-bar">
<div class="left"></div>
<div class="right"></div>
</div>
<!--End Yellow-bar-->
<!--####################################################### Start bg-testi-->
<div class="bg_signup">

	<!--Start Form-->
<div id="main" style="margin:auto;">
<div class="form_title">
<h2>User Login</h2>
<p>Join Wahegurujaap today and unlimited get rewards </p>
</div>
<p class="alert alert-success" id="success2"></p>
<p class="alert alert-warning" id="error2"></p>
<form id="login-page" action="{{ url('userlogin') }}" method="POST">	{{ csrf_field() }}  
<div class="textfield">
    <label>Username</label>     <input type="text" name="username" value="" placeholder="Choose Your Username" />  </div>
   <div class="textfield">
    <label>Password</label>     <input type="password" name="password" value="" placeholder="Enter a password" id="pass" />  </div>

  <div class="textfield">
    <label>&nbsp;</label>
    <input type="submit" style="display:none" />
     <button name="mybutton" type="submit" class="btn btn-warning" id="mybutton">Login</button>   </form>  </div>

<div class="clear"></div>
</div>
<!--End form-->


</div>
<!--####################################################### End tart bg-testi-->

</div></div>
<script type="text/javascript">

	$(document).ready(function() {
	    $.ajaxSetup({
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            }
	        });

	        $("#login-page").on('submit', function(e){
	            e.preventDefault();

	        $.ajax({
	            type: "POST",
	            url:  "{{   url('userlogin') }}",
	            data: $(this).serializeArray(),
	            contentType: $(this).data('type'),
	            success: function (data) {
                	msg = JSON.parse(data);	            	
	                if(msg.message=='success') {
                    $('#error2').css('display', 'none');                                      
                    $('#success2').css('display', 'block');                                               	
	                    $('#success2').html('loging in..');
	                    window.location.href = "{{ url('/profile') }}/"+msg.username;
	                } else {
                    $('#error2').css('display', 'block');                                      

	                    $('#error2').html(msg.message);                  
	                }
	            }
	        });
	     });
	});

</script>
@endsection