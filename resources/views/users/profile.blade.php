@extends ('layouts.master')

@section('content')
<!-- <p>commit chnages</p> -->
<div class="yellow-bar">
<div class="left"></div>
<div class="right"></div>
</div>
<!--End Yellow-bar-->
<!--Start Left Box-->
<img id="uploadPreview" style="display:none;"/>
<div class="left-box">

<h2><span>{{ Auth::user()->firstname }}</span> Profile</h2>
<div class="boxx">
<div class="bar">
<h3>Member Profile</h3>
<h2>Total Jaap <span>( {{ Auth::user()->jaap }} )</span></h2>
</div>

<div class="uploadd_img">
<img src="" alt=""/>
@if(! Auth::user()->image)
<img src="http://wahegurujaap.com/resources/front/images/profile_default.jpg" width="205" height="210" class="img" alt=""/>
@else
<img src="{{ asset('assets/members/' . Auth::user()->image) }}" width="205" height="210" class="img" alt=""/>
@endif
@if(count($errors))
<ul class="alert alert-danger" style="list-style-type: none;">
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
@endif
@if(Session::has('message')) <div class="alert alert-info"> {{Session::get('message')}} </div> @endif
<form action="{{ url('/profile/' . Auth::user()->username ) }}/upload" method="post" accept-charset="utf-8" enctype="multipart/form-data">
{{ csrf_field() }}
<input type="file" name="uploadImage" value="" id="uploadImage" accept="image/jpeg"  /><p style="color:red">Max size (1 MB 1000 X 1000)</p>
<input type="submit" name="submit" value="Upload" class="btn btn-warning" />

</form>
</div>
 <fieldset class="infoo">
  <legend><h3>Basic Information</h3></legend>
  
<ul>
<li><span>User Name :</span> <font>{{ Auth::user()->username }}</font></li>
<li><span>First Name :</span> <font>{{ Auth::user()->firstname }}</font></li>
<li><span>Last Name :</span> <font>{{ Auth::user()->lastname }}</font></li>
<li><span>Mobile :</span> <font>+{{ Auth::user()->mobile }}</font></li>
<li><span>D.O.B :</span> <font>0000-00-00</font></li>
<li><span>Location :</span> <font>  </font></li>
<li><span>Winner of the week :</span> <font>0 Times</font></li>
<li><span>About :</span> <font></font></li>
</ul>
 </fieldset>
<div class="clear"></div>
</div>
</div>
<!--End Left Box-->
<!--Start Right Box-->
@include('layouts.profile_sidebar')
@endsection