@extends ('layouts.master')

@section ('content')
	<div class="yellow-bar">
<div class="left"></div>
<div class="right"></div>
</div>
<!--End Yellow-bar-->
<h2>Gurudwara List</h2>
<div class="clear"></div>
<div class="left-box">

@if(count($gurudwaras)>0)
@foreach($gurudwaras as $gurudwara)
<div class="ragi_list">
<a href=""><img src="{{ asset('/assets/gallery/' . $gurudwara->image) }}" class="img-rounded" width="80" height="60" id="ragi_icon" alt=""/></a><table border="0">
<tr>
<td>
<h3><a href="{{ url('/gurudwara/' . $gurudwara->id) }}">Gurdwara {{ $gurudwara->name }}</a></h3>
<div class="clear"></div>
<p style="width:auto">
{{ $gurudwara->info }}</p>
</td>
<td>
<h3 style="text-align:center;float:none;margin: -15px 0px 10px 0px;"></h3>
<p style="width:80px;">
<span></span> 
</p>
</td>
</tr>
</table>
<div class="bar">
<h3 style="margin-top:7px;"></h3>
<h2>Reviews : <span>{{ count($gurudwara->comments) }}</span></h2>
</div>
</div>
@endforeach
@else
	<p>No item found.</p>
@endif


</div>
</div>
@endsection