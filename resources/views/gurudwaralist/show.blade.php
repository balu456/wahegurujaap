@extends ('layouts.master')

@section ('content')
<div class="left-box">
<h2>{{ $gurudwara->name }}</h2>
<div class="clear"></div>
<div class="boxx">
<div class="bar">
<h3 style="margin-top:7px;">
<div class="fb-like" data-href="http://wahegurujaap.com/gurdwara/index/gurdwara-shaheed-baba-deep-singh-ji" data-width="100" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
<!-- Place this tag where you want the share button to render. -->
<div class="g-plus" data-action="share" data-annotation="bubble" data-height="24"></div>
<!-- Place this tag after the last share tag. -->
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
<a href="https://twitter.com/wahegurujaap" class="twitter-follow-button" data-show-count="false">Follow @wahegurujaap</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
</h3>
<h2>Reviews: <span>{{ count($gurudwara->comments) }}</span></h2>
</div>
<img src="{{ asset('/assets/gallery/' . $gurudwara->image) }}" class="img" width="205" height="210" alt=""/> <fieldset class="infoo">
  <legend><h3>Basic Information</h3></legend>
  <p>{{ $gurudwara->info }}</p>
 </fieldset>
</div>
</div>
<!--End Left Box-->
<div class="bar">
<h3>Gurdwara</h3>
<h2>Total Gurdwaras : <span>( {{ count($gurudwaras) }} )</span></h2>
</div>
<!----End bar---->
<!--######################### thumbnail-->
<style>
.well{width:96.3%;background:none;border:0;}
.span3{background:none;}
.span3 h3 {
text-align: center;
font-size: 16px;
padding: 0;
margin: 0;
color: #3A3A3A;
}
.dis {
line-height: 18px;
font-size: 11px;
margin: 0;
padding: 0;
float: left;
text-align: center;
}
</style>
<div class="video-gallery">  
        
    <div class="well">
     
    <div id="myCarousel" class="carousel slide">
     
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
     
    <!-- Carousel items -->
    <div class="carousel-inner">

    <div class="item active">
        <div class="row-fluid">
        @if(count($gurudwaras))
        @foreach($gurudwaras as $g)
            <div class="span3">
            <a href="{{ url('/gurudwara/' . $g->id) }}" class="thumbnail"><img src="{{ asset('/assets/gallery/' . $g->image) }}" width="100%" height="100%" alt=""/></a><h3>{{ $g->name }}</h3>
            <p class="dis"></p>
            </div>
        @endforeach
        @endif
    </div>
    </div>

    </div><!--/carousel-inner-->
     
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
    </div><!--/myCarousel-->
     
    </div><!--/well-->
    
 </div>   
<div class="timelinee" id="review">
<div class="bar">
<h3>Enter Your Comments</h3>
<h2>Total Reviews : <span>( {{ count($gurudwara->comments) }} )</span></h2>
</div>
<div class="left-box">
<!--Start boxx here-->
<div class="boxx">
<form action="{{ url('/gurudwara/' . $gurudwara->id . '/comment') }}" method="post" accept-charset="utf-8"><textarea name="review" id="review" class="jaap-field" placeholder="Enter Your Comments" ></textarea>
{{ csrf_field() }}
<button name="submit" type="submit" class="btn btn-info" id="my-button2">Submit</button></form></div>
<!--End boxx here-->
</div>
<div class="clear"></div>
    <div class="qa-message-list" id="wallmessages">
    @foreach($gurudwara->comments as $comment)
<div class="message-item" id="m16">
    <div class="message-inner">
        <div class="message-head clearfix">
            <div class="avatar pull-left"><a href="{{ url('/user/' . $comment->user->username)  }}"><img src="http://wahegurujaap.com/resources/front/images/profile_default.jpg" alt=""></a></div>
            <div class="user-detail">
                <h5 class="handle">{{ $comment->user->firstname }}</h5>
                <div class="post-meta">
                    <div class="asker-meta">
                        <span class="qa-message-what"></span>
                        <span class="qa-message-when">
                            <span class="qa-message-when-data">{{ $comment->created_at->format('F d, Y') }}</span>
                        </span>
                        <span class="qa-message-who">
                            <span class="qa-message-who-pad">by </span>
                            <span class="qa-message-who-data"><a href="{{ url('/user/' . $comment->user->username)  }}">{{ $comment->user->firstname }}</a></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="qa-message-content">{{ $comment->body }}</div>
</div>
</div>
@endforeach
    
</div>
</div>
@endsection