@extends ('admin.master')
@section('master_content')

    <!-- Main content -->
<section class="content">

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Hover Data Table</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             @if(Session::has('success')) <div class="alert alert-info"> {{Session::get('success')}} </div> @endif
              <table id="example2" class="table table-bordered table-hover">
                <thead>

                <tr>
                  <th>Name</th>
                  <th>Image</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>

                </thead>
                <tbody>
            @if($gurudwaras)
              @foreach($gurudwaras as $item)
                <tr>
                  <td>{{ $item->name }}</td>
                  <td><img src="{{ asset('assets/gallery/' . $item->image) }}" height="150px" width="150px">
                  </td>
                  <td> <a href="{{ url('/admin/ragi/edit/' . $item->id) }}"> edit </a> </td>
                  <td> <a href="{{ url('/admin/ragi/delete/' . $item->id) }}"> delete </a> </td>
                </tr>
              @endforeach
            @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>Title</th>
                  <th>Image</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

         
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

</section>
    <!-- /.content -->
  </div>
@endsection