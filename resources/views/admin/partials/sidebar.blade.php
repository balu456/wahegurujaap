  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset("/admin-lte/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Hi Admin!</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">HEADER</li>
        <!-- Optionally, you can add icons to the links -->
        <li {{ (Request::is('admin/dashboard') ? 'class=active' : '') }}><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-link"></i> <span>Dashboard</span></a></li>
        <li {{ (Request::is('admin/users') ? 'class=active' : '') }}><a href="{{ url('admin/users') }}"><i class="fa fa-link"></i> <span>Users</span></a></li>
        <li class="{{ (Request::is('/gallery/add') ? 'active' : '') }} treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Gallery</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li {{ (Request::is('admin/gallery/add') ? 'class=active' : '') }}><a href="{{ url('admin/gallery/add') }}">Add gallery</a></li>
            <li {{ (Request::is('/admin/gallery/view') ? 'class=active' : '') }}><a href="{{ url('admin/gallery') }}">View gallery</a></li>
          </ul>
        </li>
        <li class="{{ (Request::is('/gallery/add') ? 'active' : '') }} treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Videos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li {{ (Request::is('admin/gallery/add') ? 'class=active' : '') }}><a href="{{ url('admin/videos/add') }}">Add video</a></li>
            <li {{ (Request::is('/admin/gallery/view') ? 'class=active' : '') }}><a href="{{ url('admin/videos') }}">View videos</a></li>
          </ul>
        </li>
        <li class="{{ (Request::is('/ragi/add') ? 'active' : '') }} treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Ragis</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li {{ (Request::is('admin/ragi/add') ? 'class=active' : '') }}><a href="{{ url('admin/ragi/add') }}">Add ragi</a></li>
            <li {{ (Request::is('/admin/ragis') ? 'class=active' : '') }}><a href="{{ url('admin/ragis') }}">View ragis</a></li>
          </ul>
        </li>
        <li class="{{ (Request::is('/gurudwara/add') ? 'active' : '') }} treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Gurudwaras</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li {{ (Request::is('admin/gurudwara/add') ? 'class=active' : '') }}><a href="{{ url('admin/gurudwara/add') }}">Add gurudwara</a></li>
            <li {{ (Request::is('/admin/gurudwara/view') ? 'class=active' : '') }}><a href="{{ url('admin/gurudwaras') }}">View gurudwaras</a></li>
          </ul>
        </li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>
