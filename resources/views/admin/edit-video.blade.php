@extends ('admin.master')
@section('master_content')
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Video
        <!-- <small>Preview</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">video</a></li>
        <li class="active">edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <!-- <h3 class="box-title">Add image</h3> -->
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            @if(Session::has('success')) <div class="alert alert-info"> {{Session::get('success')}} </div> @endif
            @if(count($errors))
              <ul>
              @foreach($errors->all() as $error)
                <li class="alert-danger danger">{{ $error }}</li>
              @endforeach
              </ul>
            @endif
            <form role="form" action="{{ url('/admin/videos/update/' . $video->id ) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label for="title">Title</label>
                  <input type="text" class="form-control" name="title" id="title" placeholder="Enter title" value="{{ $video->title }}">
                </div>  
                <div class="form-group">
                  <label for="title">Video link</label>
                  <input type="text" class="form-control" name="video_link" id="video_link" value="{{ $video->video_link }}" placeholder="Enter video link">
                </div>  
                <div class="form-group">
                  <label for="title">Ragis</label>
                  <select class="form-control" id="sel1" name="ragi_id">                  
                    @foreach($ragis as $ragi)
                      <option value="{{ $ragi->id }}" @if($ragi->id==$video->ragi_id) selected @endif>{{ $ragi->name }}</option>
                    @endforeach

                  </select>
                </div>
                <div class="form-group">
                  <label for="title">Video description</label>
                  <textarea class="form-control" rows="4" cols="50" placeholder="Enter video description" name="description">{{ $video->description }}</textarea>
                </div>  
                <div class="form-group">
                  <img src="{{ asset('assets/gallery/' . $video->image) }}" width="150" height="150"><br />
                  <label for="exampleInputFile">File input</label>
                  <input type="file" name="image" id="exampleInputFile">
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        <!--/.col (right) -->
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection