@extends ('admin.master')
@section('master_content')
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header)  -->
    <section class="content-header">
      <h1>
        Add gallery image
        <!-- <small>Preview</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">gallery</a></li>
        <li class="active">add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <!-- <h3 class="box-title">Add image</h3> -->
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            @if(Session::has('success')) <div class="alert alert-info"> {{Session::get('success')}} </div> @endif
            <form role="form" action="{{ url('/admin/gallery/add') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @if(count($errors))
              <ul>
              @foreach($errors->all() as $error)
                <li class="alert-danger danger">{{ $error }}</li>
              @endforeach
              </ul>
            @endif
              <div class="box-body">
                <div class="form-group">
                  <label for="title">Title</label>
                  <input type="text" class="form-control" value="{{ old('title') }}" name="title" id="title" placeholder="Enter title">
                </div>    
                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="file" name="image" id="exampleInputFile">
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        <!--/.col (right) -->
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection