@extends ('admin.master')
@section('master_content')

    <!-- Main content -->
<section class="content">

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">users</a></li>
        <!-- <li class="active">Data tables</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">All users</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             @if(Session::has('success')) <div class="alert alert-info"> {{Session::get('success')}} </div> @endif
              <table id="example2" class="table table-bordered table-hover">
                <thead>

                <tr>
                  <th>Firstname</th>
                  <th>Lastname</th>
                  <th>Email</th>
                  <th>Mobile</th>
                  <th>Delete</th>
                </tr>

                </thead>
                <tbody>
            @if($users)
              @foreach($users as $user)
                <tr>
                  <td><a href="{{ url('/user/' . $user->username) }}" target="_blank">{{ $user->firstname }}</a></td>
                  <td>{{ $user->lastname }}</td>
                  <td>{{ $user->email }}</td>
                  <td>{{ $user->mobile }}</td>
                  <td> <a href="{{ url('/admin/user/delete/' . $user->id) }}"> delete </a> </td>
                </tr>
              @endforeach
            @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>firsname</th>
                  <th>lastname</th>
                  <th>email</th>
                  <th>mobile</th>
                  <th>delete</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

         
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

</section>
    <!-- /.content -->
  </div>
@endsection