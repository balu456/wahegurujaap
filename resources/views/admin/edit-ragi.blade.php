@extends ('admin.master')
@section('master_content')
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Ragi
        <!-- <small>Preview</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">ragi</a></li>
        <li class="active">edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <!-- <h3 class="box-title">Add image</h3> -->
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            @if(Session::has('success')) <div class="alert alert-info"> {{Session::get('success')}} </div> @endif
            @if(count($errors))
              <ul>
              @foreach($errors->all() as $error)
                <li class="alert-danger danger">{{ $error }}</li>
              @endforeach
              </ul>
            @endif
            <form role="form" action="{{ url('/admin/ragi/update/' . $ragi->id ) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label for="title">Name</label>
                  <input type="text" class="form-control" name="name" id="title" placeholder="Enter title" value="{{ $ragi->name }}">
                </div>  
                <div class="form-group">
                  <label for="title">Ragi description</label>
                  <textarea class="form-control" rows="4" cols="50" placeholder="Enter video description" name="description">{{ $ragi->description }}</textarea>
                </div>  
                <div class="form-group">
                  <img src="{{ asset('assets/gallery/' . $ragi->image) }}" width="150" height="150"><br />
                  <label for="exampleInputFile">File input</label>
                  <input type="file" name="image" id="exampleInputFile">
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        <!--/.col (right) -->
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection