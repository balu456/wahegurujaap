<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|-------------------------------------------------------------------------- commiting changes
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// front-end routes 

// API v1

$path = 'localhost/waheguru/public/assets/gallery';
Route::get('v1/auth/users', function(){
    
    $users = App\User::all();
    return $users;
});

Route::get('v1/auth/gallery', function(){
    
    $g = App\Gallery::all();

    return $g;
    // return "<img src='public/assets/gallery/".$g->image."'>";
});

Route::get('shop', 'ProductController@index');

Route::get('cart', function(){
    return view('shop.cart');
});

Route::get('shop/add-to-cart/{id}', 'ProductController@addToCart');

Route::post('shop/update-cart/{id}', 'ProductController@updateCart');

Route::get('cart/destroy/{id}', 'ProductController@destroyCart');

Route::get('/', 'HomeController@index');

Route::get('/gallery', 'GalleryController@index');

Route::get('/gallery/{id}', 'GalleryController@show');

Route::post('/gallery/{id}/comment/', 'CommentsController@comment');

Route::get('/videos', 'VideoController@index');

Route::get('/video/{id}', 'VideoController@show');

Route::post('/video/{id}/comment/', 'CommentsController@comment');

Route::get('/winners', 'WinnerListController@index');

Route::get('/ragis', 'RagiListController@index');

Route::get('/ragi/{id}', 'RagiListController@show');

Route::post('/ragi/{id}/comment', 'CommentsController@comment');

Route::get('/gurudwaras', 'GurudwaraListController@index');

Route::get('/gurudwara/{id}', 'GurudwaraListController@show');

Route::post('/gurudwara/{id}/comment', 'CommentsController@comment');

Route::get('/kirtan', function(){
	return view('kirtan');
});

Route::get('/users', 'UsersListController@index');
Route::get('/contact', 'ContactController@index');


Route::get('/user/{id}', 'UsersListController@show');

Route::post('/contact/send', 'ContactController@sendContact');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/jaap', 'HomeController@jaap')->name('home');

// user auth
Route::post('/register', 'RegisterController@store');

Route::get('/activate/{token}', 'RegisterController@activate');

Route::post('/activate', 'RegisterController@activation');

Route::get('/activate', function(){
    return view('users.activate');
});

Route::get('/login', 'MemberController@index');

Route::get('/logout', 'MemberController@logout');

Route::get('/profile/{user}', 'MemberController@profile')->middleware('user_auth');

Route::middleware('user_auth')->prefix('/profile/{user}')->group(function() {
    
    Route::get('/comments', 'MemberController@comments');        
    
    Route::get('/edit', function(){
        $user = Auth::User();
        return view('users.edit', compact('user'));
    }); 

    Route::get('/reset', function(){
        return view('users.reset');
    });   

    Route::post('reset', 'MemberController@reset');  

    Route::post('upload', 'MemberController@upload');

    Route::get('/jaaps', function(){
        $user_id = Auth::User()->id;                               
        $user_jaaps = App\Jaap::where('user_id','=', $user_id)->get();

        return view('users.jaaps', compact('user_jaaps'));
    });   

    Route::get('/jaap', function(){
        return view('users.jaap');
    });


    Route::post('/update', 'MemberController@update');

});

Route::post('/userlogin', 'MemberController@authenticate');

Route::get('/404', function(){
    return view('404');
});


//backend routes

Route::middleware('admin')->get('admin', function(){

        return  view('admin.dashboard');
});

Route::get('admin/login', function(){
    if(Auth::check()) {

        return view('admin.dashboard');        
    }
    return view('admin.login');
    
});

Route::post('adminlogin', 'AdminController@index');

Route::middleware('admin')->prefix('admin')->group(function () {

	Route::get('users', function(){
    		
    		$users = App\User::all();

    		return view('admin.users', compact('users'));
    });

	Route::get('user/delete/{id}', function($id){
    		
    		$user = App\User::findorFail($id);

    		$user->delete();
    		
    		Session::flash('success', 'User deleted');
    		return back();
    });

    Route::get('dashboard', function () {

        return view('admin.dashboard');
    });

    Route::get('gallery', function () {

    	$gallery = App\Gallery::orderBy('created_at', 'desc')->get();

        return view('admin.gallery', compact('gallery'));
    });

    Route::prefix('gallery')->group(function() {

    	Route::get('add', function () {
	        return view('admin.add-gallery');
	    });

	    Route::get('edit/{item}', 'GalleryController@edit');

	    Route::post('add', 'GalleryController@add');
	    
	    Route::post('update/{id}', 'GalleryController@update');

	    Route::get('delete/{id}', 'GalleryController@delete');

    });

    Route::get('videos', function () {

        $videos = App\Video::orderBy('created_at', 'desc')->get();

        return view('admin.videos', compact('videos'));
    });

    Route::prefix('videos')->group(function() {

        Route::get('add', function () {

            $ragis = App\Ragi::all();

            return view('admin.add-video', compact('ragis'));
        });

        Route::get('edit/{item}', 'VideoController@edit');

        Route::post('add', 'VideoController@add');
        
        Route::post('update/{id}', 'VideoController@update');

        Route::get('delete/{id}', 'VideoController@delete');

    });

    Route::get('ragis', function () {

        $ragis = App\Ragi::orderBy('created_at', 'desc')->get();

        return view('admin.ragis', compact('ragis'));
    });

    Route::prefix('ragi')->group(function() {

        Route::get('add', function () {
            return view('admin.add-ragi');
        });

        Route::post('add', 'RagiListController@add');
        
        Route::get('edit/{item}', 'RagiListController@edit');

        Route::post('update/{id}', 'RagiListController@update');

        Route::get('delete/{id}', 'RagiListController@delete');

    });

    Route::get('gurudwaras', function () {

        $gurudwaras = App\Gurudwara::orderBy('created_at', 'desc')->get();

        return view('admin.gurudwaras', compact('gurudwaras'));
    });

    Route::prefix('gurudwara')->group(function() {

        Route::get('add', function () {
            return view('admin.add-gurudwara');
        });

        Route::post('add', 'GurudwaraListController@add');
        
        Route::get('edit/{item}', 'GurudwaraListController@edit');

        Route::post('update/{id}', 'GurudwaraListController@update');

        Route::get('delete/{id}', 'GurudwaraListController@delete');

    });

    Route::get('logout', 'AdminController@logout');

});

